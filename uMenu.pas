unit uMenu;

interface

uses WinCrt, WinGraph, uWorld, uButton;

const M=6;

type
 TName = record
   Num: integer;
   Name:String [26];
   Path:String [80];
 end;
 TDataFile = file of TName;
 TNameArr = array [0..M] of String[40];

  TMenu = object
    private
      X, Y, Height, Width :integer;
      Name: string [40];
    public
      constructor Init;
      procedure Show; virtual;
      function Selecting:integer;
      procedure Hide;
      destructor Term; virtual;
  end;

  TMainMenu = object (TMenu)
    private
      Arr: array [1..10] of TButton;
      ButNum,Selected :integer;
    public
      constructor Init(NameArr:TNameArr);
      procedure Show; virtual;
      function OnClick:integer;
  end;
  
  TOpenFileMenu = object (TMenu)
    private
      Arr: array [1..40] of TLineButton;
      DataFile: TDataFile;
      First, Selected, FSize, bNum: integer;
    public
      constructor Init(aDataPath:string);
      procedure Show; virtual;
      function OnClick:integer;
      destructor Term; virtual;
  end;
  
  TTextWindow = object (TMenu)
    private
      But: TButton;
      f: text; 
      Path: string;
      procedure Text;
    public
      constructor Init(aPath:string);
      procedure Show; virtual;
  end;
  
  TSaveFileMenu = object (TMenu)
    private
      OkButton, BackButton: TButton;
      FileNameField: TTextField;
      Arr: array [1..3] of TButton;
      Selected: integer;
    public
    Function Fill (var aStr: string): char;
      constructor Init; 
      procedure Show; virtual;
      function OnClick(var aStr:string):boolean;
      
  end;  
  
implementation

const
  WindowColor=$C0C0C0;{Silver}
  TextColor=$000000;{Black}
  NameColor=$000000;{Black}
  ButtonColor=$D1BEAD;{Casper}
  FrameColor=$545454; {Dark Gray}
const
  FontSize=18;
  StringHeight=25;
  FontName='Georgia';
  MenuFile=String ('Data\Menu.dat');
  
constructor TMenu.Init;
begin
  X:=GetMaxX div 3;
  Y:=0;
  Width:=GetMaxX div 3;
  Height:= GetMaxY;
end;

function TMenu.Selecting:integer;
var Chr: Char;
begin
  Selecting:=0;
  repeat
    Chr:=readkey;
    if (Chr>=#48) and (Chr<=#57) then
      Selecting:=Ord(Chr)-48
    else
      case Chr of
       Up: begin  end;
       Down: begin  end;
      end;
  until Selecting<>0;
end;

procedure TMenu.Show;
begin
  SetViewPort (X, Y, X+Width, Y+Height, ClipOn);
  SetFillStyle (SolidFill, WindowColor);
  FillRect (0,0,Width, Height);
  SetColor (TextColor);
  Line (0,20,Width,20);
end;
procedure TMenu.Hide;
begin
  setfillstyle (SolidFill, GetBkColor);
  bar (0,0, Width, Height);
  SetScreenMax;
end;
destructor TMenu.Term;
begin
end;


constructor TMainMenu.Init(NameArr:TNameArr);
var i,bX,bY, ButHeight, ButWidth:integer;
begin
  inherited Init;
  Name:=NameArr[0];
  bX:=Width div 10;
  bY:=40;
  ButWidth:=Width-2*bX;
  ButHeight:= StringHeight;  
  i:=1;
  while ((Length(NameArr[i]))<>0) and (i<=M) do begin
    Arr [i].Init (bX,bY,ButWidth,ButHeight,NameArr[i]);
    Inc (bY, ButHeight +10);
    Inc (ButNum);
    Inc (i);
  end;
  Selected:=1;
end;

procedure TMainMenu.Show;
var i:integer;
begin
inherited Show;
SetColor (TextColor);
  SetTextJustify (CenterText, TopText);
  OutTextXY (Width div 2,3, Name);
for i:=1 to ButNum do
  Arr[i].Show;
end;

function TMainMenu.OnClick:integer;
var Chr: Char; i:integer;
begin
  OnClick:=0;
  I:=Selected;
  Arr[Selected].Select;
  Repeat
    Chr:=readkey;
    Arr[i].Deselect;
    begin
	    case Chr of
	      Up: begin Dec(i); end;
	      Down: begin Inc(i); end;
	    end;  
	    if (i<1) then
	      i:=ButNum;
	    if (i>ButNum) then
	      i:=1;
	    Arr[i].Select; 
    end;
  until (Chr=enter) or (OnClick<>0);
  OnClick:=i;
  Selected:=i;
end;

constructor TOpenFileMenu.Init(aDataPath:string);
var i,bX,bY, ButHeight, ButWidth:integer; 
begin
  inherited Init;
  Assign (DataFile, aDataPath);
  {I-}
   Reset (DataFile);
  {I+}
  if IOResult<>0 then
    Error (Err)
  else begin
	   Name:='Open File';
	   FSize:=FileSize (DataFile);
	  Close (DataFile);
	  First:=1;
	  bX:=Width div 10;
	  bY:=30;
	  ButWidth:=Width-2*bX;
	  ButHeight:= StringHeight;  
	  if (FSize<19) then
	    bNum:=FSize-1
	  else
	    bNum:=19;
	  for i:=1 to bNum do begin
	    Arr [i].Init (bX,bY,ButWidth,ButHeight,'');
	    Inc (bY, ButHeight +5);
	  end;    
	  Selected:=1;
  end;
end;

procedure TOpenFileMenu.Show;
var Temp: TName; i:integer;
begin
  inherited Show;
  {I-}
  Reset (DataFile);
  {I+}
  if IOResult<>0 then begin
    Error(Err);
  end
  else begin
	  SetColor (TextColor);
	  SetTextJustify (CenterText, TopText);
	  OutTextXY (Width div 2,3, Name);
	  SetColor (TextColor);
	  Seek (DataFile, First);
	  i:=1;
	  while (not eof(DataFile)) and (i<20) do begin
	    read (DataFile, Temp);
	    Arr[i].SetName (Temp.Name);
	    Arr[i].Show;
	    Inc(i);
	  end;
	  Close (DataFile);
	end;
end;

function TOpenFileMenu.OnClick:integer;
  procedure ScrollUp;
  begin
    Dec (First);
    Show;
  end;
  procedure ScrollDown;
  begin
    Inc (First);
    Show;
  end;
var Ch: Char; i:integer;
begin
  OnClick:=0;
  i:=1;
  First:=1;
  Arr[i].Select;
  repeat
    ch:=readkey;
    case Ch of
      Up: begin Arr[i].Deselect;Dec(i); end;
      Down: begin Arr[i].Deselect; Inc(i); end;
    end;
    if (i<1) then begin
      if (First=1) then begin
        if (bNum<19) then
          First:=1
        else
        First:=FSize-bNum;
        i:=bNum;
        Show;
      end
      else begin
        ScrollUp;
        i:=1;
      end;
    end;
    if (i>bNum) then begin
      if (First=FSize-bNum) then begin
        First:=1;
 	    i:=1;
 	    Show;
      end
      else begin
        ScrollDown;
        i:=bNum;
      end;
    end;
    Arr[i].Select;
    Selected:=First+i-1;
  until (Ch=Enter) or (Ch=Esc);
  case ch of
    enter : OnClick:=Selected;
    esc: OnClick:=-1;
  end;
  for i:=1 to bNum do
    Arr[i].Deselect;
end;

destructor TOpenFileMenu.Term;
begin
  Close (DataFile);
end;

const  Back='Back';

constructor TTextWindow.Init (aPath:string);
var bX,bY, ButHeight, ButWidth:integer;
begin
  inherited Init;
  bX:=Width div 10;
  bY:=Height - StringHeight - 20;
  ButWidth:=Width-2*bX;
  ButHeight:=StringHeight;
  But.Init (bX,bY, ButWidth,ButHeight, Back);
  Path:=aPath
end;


 
procedure TTextWindow.Text;
var 
Temp: string; 
tX,tY: integer;
begin
  tX:=Width div 2;
  tY:=20;
  assign (f, Path);
  {I-}
  reset (f);
  {I+}
  if IOResult=0 then begin
	while not (eof(f)) do begin
	  readln (f, Temp);
	  OutTextXY (tX, tY, Temp);
	  Inc (tY, TextHeight(Temp));
	end;
	Close (f);
  end
  else
    Error (Err);
end;

procedure TTextWindow.Show;
begin
  inherited Show;
  But.Show;
  But.Select;
  Text;
end;


const Ok='OK';

constructor TSaveFileMenu.Init;
var fX,fY, fHeight, fWidth, bX,bY,
     ButWidth,ButHeight: integer;
begin
  inherited Init;
  fX:=Width div 30;
  fY:=Height div 20+30;
  fHeight:=StringHeight;
  fWidth:=Width-2*fX;
  FileNameField.Init(fX,fY, fWidth,fHeight,'');
  Name:='Save File';
  bX:=Width div 10;
  bY:=Height - StringHeight - 20;
  ButWidth:=Width div 2 - 3*bX div 2;
  ButHeight:=StringHeight;
  OkButton.Init (bX,bY, ButWidth,ButHeight,Ok);
  bX:= Width - bX - butWidth;  
  BackButton.Init (bX,bY, ButWidth,ButHeight,Back);
  Selected:=1;
      Arr[1]:=FileNameField;
      Arr[2]:=OkButton;
      Arr[3]:=BackButton;
   
end;

procedure TSaveFileMenu.Show;
var i:integer;
begin
  inherited Show;
  SetColor (TextColor);
  SetTextJustify (CenterText, TopText);
  OutTextXY (Width div 2,3, Name);
  SetTextJustify (LeftText, TopText);
  SetColor (DarkGray);
  With FileNameField do
    OutTextXY (GetX, GetY - FontSize, 'Configuration Name (22)');
  Arr[Selected].Select;
  for i:=1 to 3 do
    Arr[i].Show;
end;

function TSaveFileMenu.Fill (var aStr: string): char;
begin
  Fill:=FileNameField.Fill(aStr);
end;

function TSaveFileMenu.OnClick(var aStr:string):boolean;
var ch:char;
begin
  Arr[Selected].Select;
  repeat
    if selected=1 then begin
      ch:=FileNameField.Fill(aStr);
      end
    else
      ch:=readkey;
	    case ch of
	      down: if Selected<3 Then begin 
	         Arr[Selected].Deselect;
	         inc(Selected); 
	        end;
	      up: if Selected>1 then begin 
	         Arr[Selected].Deselect; 
	         dec (Selected); 
	        end;
	    end;
	    FileNameField.Show;
	    Arr[Selected].Select;
  until ((Selected=3) or (Selected=2)) and ((ch=Enter));
  begin
	  case Selected of
	    2: OnClick:=True;
	    3: OnClick:=False;
	  end;
  end;
end;

end.
