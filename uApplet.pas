unit uApplet;

interface
uses WinCrt, WinGraph, uNet, uWorld, uMenu, uButton;

var i : byte;

const
  MainMenuStrings : TNameArr = ('Main Menu','1.New Game',
  '2.Continue', '3. About','4.Exit','','');
  NewGameMenuStrings : TNameArr = ('New Game','1.Open Configuration',
  '2.Create Configuration', '3. Random Config','4.Edit','5.Back','');
  PauseMenuStrings : TNameArr = ('Pause Menu','1.Continue',
  '2.New Game', '3. Save File','4. Help','5. Main Menu','6. Exit');
  
const 
  DataFilePath = 'Data\Configurations.dat';
  TestPath= 'Maps\Test1.txt';
  Test2Path='Maps\wTest.txt';
  AboutFile='Data\About.dat';
  HelpFile='Data\Help.dat';
  MapsFolder='maps\';
  LastFile='Data\LastFile.lcf';

type
  TApp = object
    private
      Net: TNet;
      World : TWorld;
      MainMenu, NewGameMenu, PauseMenu: TMainMenu;
      OpenFileMenu: TOpenFileMenu;
      SaveFileMenu: TSaveFileMenu;
      About, Help: TTextWindow;
      Font : integer;
      Data: TName; {запись с именем и путем конфигурации}
      DataFile: TDataFile; {Файл с путями конфигураций}
      ToMainMenu: boolean;
      procedure NewGame;
      function OpenFile:boolean;
      procedure Game;
      procedure SaveFile;
    public
      constructor Init;
      procedure Run;
      Destructor Done;
  end;

implementation

  constructor TApp.Init;
  var Driver, Mode: integer;
  begin
    Driver:= NoPalette;
    Mode:= m800x600;
    initgraph (Driver, Mode , 'Conways Life Game');
    Font:=InstallUserFont (FontName);
    SetTextStyle (Font, 0, FontSize);
   PauseMenu.Init(PauseMenuStrings);
   OpenFileMenu.Init(DataFilePath);
   NewGameMenu.Init (NewGameMenuStrings);
   Net.Draw;
   World.Init(Net.GetY,Net.GetX, LastFile);
   World.Draw;
   MainMenu.Init(MainMenuStrings);
   SaveFileMenu.Init;
   About.Init (AboutFile);
   Help.Init (HelpFile);
  end;
    
  function TApp.OpenFile:boolean;
  var k:integer;
  begin
    NewGameMenu.Hide;
	   OpenFileMenu.Show;
	   k:=OpenFileMenu.Onclick;
	   if k<>-1 then begin
		   Assign (DataFile,DataFilePath);
		   reset (DataFile);
		   Seek (DataFile,k);
		   read (DataFile,Data);
		   Close (DataFile);
		   SetScreenMax;
		   Net.Draw;
		   World.SetFileName (Data.Path);
		   World.OpenFile;
		   ClearDevice;   
		   Net.Draw;
		   World.Draw;
		   OpenFile:=True;
	   end
	   else
	     OpenFile:=False;
  end;
  
  procedure TApp.SaveFile;
  var temp: TName; q:boolean;
  begin
    SaveFileMenu.Show;
    if SaveFileMenu.OnClick (Data.Name) then begin
	    assign (DataFile, DataFilePath);
	    {I-}
	    Reset (DataFile);
	    {I+}
	    if IOResult<>0 then
	      Error (Err)
	    else begin
	      Data.Num:=0;  
	      q:=true;
	      while not eof(DataFile) do begin
	        read (DataFile, temp);
	        if temp.Num>Data.Num then
	          Data.Num:=Temp.Num;
	        if temp.Name=Data.Name then begin
	          if q then begin
	            Data.Path:=Data.Name+'-';
	            q:=false;
	          end
	          else
	            Data.Path:=Data.Path+'-'
	        end;
	      end;
	      Data.Path:=MapsFolder+Data.Path+'.lcf';
	      Seek(DataFile, FileSize(DataFile));
	      write (DataFile, Data);
	      Close (DataFile);
	    end;
	    World.SetFileName(Data.Path);
	    World.FillFile;
	  end
	  else begin
	  end;
  end;
  
  procedure TApp.Game;
  var k:integer;
  begin
  ClearDevice;
    repeat
      World.Displays;
      PauseMenu.Show;
      k:=PauseMenu.OnClick ;
      case k of
        1: begin PauseMenu.Hide;{continue} end;
        2: begin NewGame {newGame} end;
        3: begin SaveFile{save} end;
        4: begin {help}
                Help.Show;
                readkey; 
                Help.Hide;
                k:=1;
           end;
        5: begin ToMainMenu:=False; end;   
        6: begin 
             World.SetFileName(LastFile);
             World.FillFile;
             Done;
             Halt; 
           end;
      end;
    until k<>1;
    World.SetFileName (LastFile);
    World.FillFile;
  end;
  
  procedure TApp.NewGame;
  var j:integer;
  begin
	repeat
    if ToMainMenu then begin
	    NewGameMenu.Show;
	    j:=NewGameMenu.OnClick;
	    case j of
	      1: begin 
	           if OpenFile then
	             Game;
	         end;
	      2: begin {Fill New Game}
	           ClearDevice;
	           NewGameMenu.Hide; 
	           Net.Draw;     
	           World.SetNew (True);
		         World.FillArrHand;
		         Game;
	         end;
	      3: begin {random fill};
	           NewGameMenu.Hide;
	           ClearDevice;
	           Net.Draw;
	           World.FillArray;
	           World.Draw;
	           readkey;
	           Game;
	         end;
	      4: begin  {edit}
	           if OpenFile then begin
		           World.SetNew(False);
		           World.FillArrHand;
		           readkey;
		           Game;
	           end;
	         end;
	      5: begin {back} end;
	    end;
    end
    else 
      j:=5;  
	until j=5;
  end;
  
  procedure TApp.Run;
  var t: integer;
  begin
    repeat
    MainMenu.Show;
    ToMainMenu:=True;
      case MainMenu.OnClick of
        1: NewGame;
        2: begin 
            MainMenu.Hide;
            World.OpenFile;
            Game;
           end;
        3: begin {about}
             MainMenu.Hide;
             About.Show;
             readkey;
           end;
        4: t:=4; 
      end;
    until t=4;
  end;
  
  destructor TApp.Done;
  begin
    CloseGraph;
  end;
end.

