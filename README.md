John Conway's Game of Life written on FreePascal using WinGraph unit for painting.

It was my first 'big' project - cource work on first year of education in IASA KPI.

### How do I get set up? ###

For building you need FreePascal at least 2.x (2.4 preffered) and [WinGraph](http://math.ubbcluj.ro/~sberinde/wingraph/) unit.

At least, binary *.exe file is presented. It works under win7 and kubuntu 14.04 with winehq.