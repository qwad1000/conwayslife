unit uWorld;

interface

uses WinGraph, WinCrt, uNet, uButton;

const  
  CellColor=$00AA66;
type
  TArray = Array [1..M, 1..M] of 0..1;
  TWorld = object
    private
      Arr : TArray;
      Row, Col, Alive : integer;
      FileName : String;
      TextFile : Text;
      New: boolean;
      function Check (x,y: integer): byte;
      procedure Clear;
    public
      constructor Init (aRow, aCol: integer; aFileName: string);
      procedure FillArray;
      procedure FillFile;
      procedure OpenFile;
      procedure Draw;
      procedure Hide;
      procedure FillArrHand;
      procedure SetNew(aNew:boolean);
      Procedure Next;
      procedure Displays;
      procedure SetFileName(aFileName:string);
  end;

implementation
var Cursor:TCursor;

constructor TWorld.Init (aRow, aCol: integer; aFileName: string);
begin
  Row:=aRow;
  Col:=aCol;
  FileName:=aFileName;
  New:=False;
end;

procedure TWorld.Clear;
var i, j: integer;
begin
  for i:=1 to M do
    for j:=1 to M do
      Arr[i,j]:=0;
end;

procedure TWorld.FillArray;
var i,j:integer;
begin
Clear;
randomize;
for i:=1 to Col do 
  for j:=1 to Row do begin
    if random>0.7 then
      Arr[i,j]:=1
    else 
      Arr [i,j]:=0;
  end;
end;

procedure TWorld.OpenFile;
var i, j, k, l:integer;
 Str: string; 
 Height, Width: integer;
begin
  Clear;
  Height:=0;
  Assign (TextFile, FileName);
  {I-}
  Reset (TextFile);
  {I+}
  if IOResult<>0 then 
    Error (Err)
  else begin 
    while not eof (TextFile) do begin
      readln (TextFile, Str);
      Inc (Height);
    end;
    Width:=Length (Str);
    Reset (TextFile);
    k:= Col div 2 - Width div 2;
    l:=Row div 2 - Height div 2;
		For j:=1 to Height do begin
		  readln (TextFile, Str);
			For i:=1 to Width do begin
			  if Str[i]=#032 then
				Arr[i+k,j+l]:=0
			  else
				Arr[i+k,j+l]:=1;
			end;
	  end;  
	  Close (TextFile);
	end;
end;

procedure TWorld.FillFile;
var i,j,minRow,minCol,maxCol,maxRow: integer;
begin
  minRow:=Row; {Система для выделения куска массива,}
  minCol:=Col;  {который буедт записываться в файл}
  maxCol:=1;
  maxRow:=1;
  for j:=1 to Col do begin
    for i:=1 to Row do begin
      If (Arr[j,i]=1) then begin
        if (j<minCol) then
          minCol:=j;
        if (i<minRow) then
          minRow:=i;
        if (i>maxRow) then
          maxRow:=i;
        if (j>maxCol) then
          maxCol:=j;
      end;
    end;
  end;
  Assign (TextFile, FileName); {ВВедение в файл}
  {I-}
  rewrite (TextFile);
  {I+}
  if IOResult<>0 then
    Error (Err)
  else begin
	  for j:=minRow to maxRow do begin
	    for i:=minCol to maxCol do begin
	      if Arr [i,j]=0 then
	        write (TextFile, ' ')
	      else 
	        write (TextFile, #219);
	    end;
	    writeln(TextFile); 
	  end;
	  for i:=minCol to maxCol do
	    Write (TextFile,' ');
	  Close (TextFile);
  end;
end;

const R=5;

procedure TWorld.Draw;
var i, j: integer;
begin
  SetFillStyle(SolidFill,CellColor);
  for i:=1 to Col do begin
    for j:=1 to Row do begin
      if Arr [i,j]=1 then
        bar (i*R+1-R,j*R+1-R,R*i-1,R*j-1);
    end;
  end;
end;

procedure TWorld.Hide;
var i, j: integer;
begin
  SetFillStyle(SolidFill,Black);
  for i:=1 to Col do begin
    for j:=1 to Row do begin
       if Arr [i,j]=1 then
         bar (i*R+1-R,j*R+1-R,R*i-1,R*j-1);
    end;
  end;
end;

Function TWorld.Check (x,y : integer): byte;  
begin
  Check:=0;
  If (X=1) and (Y=1) then //Left Up Corner
    Check:=Arr[Col,Row]+Arr[1,Row]+Arr[2,Row]
    +Arr[Col,1]+Arr[2,1]+Arr[Col,2]+Arr[1,2]+Arr[2,2]
  else
  If (X=Col) and (Y=1) then //Right Up Corner
    Check:=Arr[1,Row]+Arr[Col,Row]+Arr[Col-1,Row]
    +Arr[1,1]+Arr[Col,1]+Arr[Col-1,2]+Arr[1,2]+Arr[2,2]
  else  
  If (X=1) and (Y=Row) then //Left Down Corner
    Check:=Arr[Col,1]+Arr[1,1]+Arr[2,1]+Arr[Col,Row-1]
    +Arr[1,Row-1]+Arr[2,Row-1]+Arr[Col,Row]+Arr[2,Row]
  else
  If (X=Row) and (Y=Col) then //Right Down Corner
    Check:=Arr[1,Row-1]+Arr[Col-1,Row-1]+Arr[Col,Row-1]
    +Arr[Col-1,Row]+Arr[1,Row]+Arr[1,1]+Arr[Col-1,1]+Arr[Col,1]
  else
  If (Y=1) then
    Check:=Arr[X-1,1]+Arr[X+1,1]+Arr[X-1,2]+Arr[X,2]
    +Arr[X+1,2]+Arr[X-1,Row]+Arr[X,Row]+Arr[X+1,Row]
  else
  If (Y=Row) then
    Check:=Arr[X-1,1]+Arr[X,1]+Arr[X+1,1]+Arr[X-1,Row-1]
    +Arr[X,Row-1]+Arr[X+1,Row-1]+Arr[X-1,Row]+Arr[X+1,Row]
  else
  If (X=1) then
    Check:=Arr[Col,Y-1]+Arr[Col,Y]+Arr[Col,Y+1]
    +Arr[1,Y-1]+Arr[1,Y+1]+Arr[2,Y-1]+Arr[2,Y]+Arr[2,Y+1]
  else
  If (X=Col) then
    Check:=Arr[1,Y-1]+Arr[1,Y]+Arr[1,Y+1]+Arr[Col-1,Y-1]
    +Arr[Col-1,Y]+Arr[Col-1,Y+1]+Arr[Col,Y-1]+Arr[Col,Y+1]
  else
  Check:=Arr[x-1,y-1]+Arr[x-1,y]+Arr[x-1,y+1]+Arr[x,y-1]
  +Arr[x,y+1]+Arr[x+1,y-1]+Arr[x+1,y]+Arr[x+1,y+1];
end;


procedure TWorld.FillArrHand;
var i,j,ti,tj :integer;
  Chr: Char;
  dX, dY:integer;
  B,bTab:boolean;
begin
  Chr:=#00;
  i:=2;
  j:=2;
  if New then
    Clear;
  Cursor.Init(R,R,Red);
  Cursor.Draw;
  b:=true;
  bTab:=false;
  While Chr<>#27 do begin
    Chr:=ReadKey;
    ti:=i;
    tj:=j;
    dX:=0;
    dY:=0;
    Case Chr of
      Left: begin Dec(dX,R); Dec (i); end;
      Right: begin Inc (dX, R); Inc (i); end;
      Up: begin Dec (dY, R); Dec (j); end;
      Down: begin Inc(dY, R); Inc (j); end;
      Tab: bTab:=not bTab;
      Enter: begin             
               If Arr[i,j]=1 then begin
                 Arr[i,j]:=0; Dec (Alive); 
                 SetFillStyle (SolidFill, Black);
               end
               else begin
                 Arr[i,j]:=1; Inc (Alive); 
                 SetFillStyle (SolidFill, CellColor);
               end;
               bar (Cursor.GetX+1, Cursor.GetY+1,
                   Cursor.GetX+R-1, Cursor.GetY+R-1);
               B:=false;
             end; 
    end;
    if bTab then begin
      SetFillStyle (SolidFill, CellColor);
      Arr[i,j]:=1; Inc (Alive);
      bar (Cursor.GetX+1, Cursor.GetY+1,
           Cursor.GetX+R-1, Cursor.GetY+R-1);
    end;  
    if B then begin
      Cursor.MoveTo (dX, dY);
      if Arr[ti,tj]=1 then begin
        SetFillStyle (SolidFill,CellColor);
        bar (Cursor.GetX-dX+1, Cursor.GetY-dY+1,
            Cursor.GetX-dX+R-1, Cursor.GetY-dY+R-1);
      end;
    end
    else begin
      if Chr<>Enter then begin
        Cursor.Init (Cursor.GetX+dX, Cursor.GetY+dY, Red);
        Cursor.Draw;
        b:=true;
      end;
    end;
  end;
  Cursor.Hide;
  Hide;
  Draw;
end;

Procedure TWorld.Next;
var i, j: integer;
ptr: TArray;
begin
  Ptr:=Arr;
  Alive:=0;
  for i:=1 to Col do
    For j:=1 to Row do begin
	  if (Check (i,j)=3) and (Arr[i,j]=0) then begin
	    Ptr [i,j]:=1;
	  end
	  else
	    if (Check(i,j)<2) or (Check(i,j)>3) then
		  Ptr [i,j]:=0;
	  if Ptr [i,j]=1 then
	    Inc (Alive);
   end;
Arr:=Ptr;
end;

procedure TWorld.Displays;
var d:integer; page: byte; i:longint;
Ch: char; b,p: boolean;
temp,live,speed:string;
vNet:TNet;
begin
  Page:=1;
  d:=150;
  Str ((350-d):10, speed);
  i:=1;
  Ch:=#00;
  p:=false;
  vNet.Draw;
  Draw;
  repeat
    b:=true;
    if keypressed or not p then begin
      ch:=readkey;
      case Ch of
        Left: if (d<350) then Inc (D,10);
        Right: if (d>0) then Dec (d,10);
        Spase: p:= not p;  
      end;
      SetColor (GetBkColor);
      OutTextXY (GetMaxX-TextWidth(speed), 
                 GetMaxY-2*TextHeight(speed), 'Speed' + speed);
      Str (((350 - D )):10, speed);
      SetColor (White);
      OutTextXY (GetMaxX-TextWidth(speed),
                 GetMaxY-2*TextHeight(speed), 'Speed' + speed);
      b:=false;
    end
    else begin
      SetActivePage (Page);
      ClearDevice;
      vNet.Draw;
      if p then begin
        Next; Inc (i); delay (d);
      end;
      Draw;
      setcolor (white);
      Str (i:10, temp);
      OutTextXY (GetMaxX-TextWidth(temp), 
                 GetMaxY-3*TextHeight(temp), 'Iter' + Temp);
      if B then begin
        Str (((350 - D )):10, Speed);
        OutTextXY (GetMaxX-TextWidth(Speed),
                   GetMaxY-2*TextHeight(Speed), 'Speed' + Speed);
      end;
      if (d > 60) then 
        Str (Alive:10, live)
      else
      if (i mod 10 = 0)then
        Str (Alive:10, live);
      OutTextXY (GetMaxX-TextWidth(live),
                 GetMaxY-TextHeight(live), 'Alive' + live);
      SetVisualPage (Page);
      Page := 1-page;
    end;
  until (ch=Esc);
end;

procedure TWorld.SetFileName(aFileName:string);
begin
  FileName:=aFileName;
end;

procedure TWorld.SetNew(aNew:boolean);
begin
  New:=aNew;
end;
end.
