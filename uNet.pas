unit uNet;

interface

uses WinGraph, WinCrt;

type
  TNet = object
    NumX, NumY: integer;
    constructor Init(aX, aY: integer);
    procedure Draw;
    procedure Hide;
    Function GetX : integer;
    function GetY : integer;
    function GetR : integer;
  end;

  TCursor = object
    private 
      X, Y: integer;
      Color: word;
     public
      constructor Init (aX,aY: integer; aColor: word);
      procedure Draw;
      procedure Hide;
      Function GetX : integer;
      function GetY : integer;
      procedure MoveTo (dX, dY: integer);
  end;

implementation
const 
  R=5;
  NetColor=$111111;
  Net10Color=$191919;
constructor TNet.Init(aX, aY: integer);
begin
  NumX:=aX;
  NumY:=aY;
end;

Procedure TNet.Draw;
var i:integer;
begin
  i:=0;
  SetColor (NetColor);
  while i<GetMaxX do begin
    Line (i,0,i,GetMaxY);
    Inc (I,R);
    inc (NumX);  
  end;
  
  i:=0;
  while i<GetMaxY do begin
    Line (0,i,GetMaxX,i);
    Inc (I,R);
    Inc (NumY);
  end;
  SetColor (Net10Color);
  i:=0;
  While i<GetMaxX do begin
  Line (i,0,i,GetMaxY);
    Inc (I,R*10);
  end;
  i:=0;
  while i<GetMaxY do begin
    Line (0,i,GetMaxX,i);
    Inc (I,R*10);
  end;
end;

Procedure TNet.Hide;
var i:integer;
begin
  i:=0;
  SetColor (Black);
  while i<GetMaxX do begin
    Line (i,0,i,GetMaxY);
    Inc (I,R);
  end;
  i:=0;
  while i<GetMaxY do begin
    Line (0,i,GetMaxX,i);
    Inc (I,R);
  end;
end;

function TNet.GetX: integer;
begin
  GetX:=NumX;
end;
function TNet.GetY: integer;
begin
  GetY:=NumY;
end;

function TNet.GetR : integer;
begin
  GetR:=R;
end;

constructor TCursor.init(aX,aY: integer; aColor: word);
begin
  X:=aX;
  Y:=aY;
  Color:=aColor;
end;

procedure TCursor.Draw;
begin
  SetColor (Color);
  Rectangle (X, Y, X+R, Y+R);
end;

procedure TCursor.Hide;
begin
  SetColor (NetColor);
  Rectangle (X, Y, X+R, Y+R);
end;

procedure TCursor.MoveTo (dX, dY: integer);
begin
  Hide;
  Inc (X, dX);
  Inc (Y, dY);
  Draw;
end;

function TCursor.GetX : integer;
begin
  GetX:=X;
end;

function TCursor.GetY : integer;
begin
  GetY:=Y;
end;

end.
