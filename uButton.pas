unit uButton;

interface

uses WinCrt, WinGraph;
const
  FontSize=18;
  FontName='Constantia';
  Err='Error: file not found. Try to restart';

const
  M=200;
  Esc=#27;
  Left=#75;
  Right=#77;
  Up=#72;
  Down=#80;
  Enter=#13;
  BkSpase=#8;
  Tab=#9;
  Spase=#32;
  
type
  TButton = object
    private 
      X, Y, Width, Height: integer;
      Selected: boolean;
      Name: string[22];
    public
      constructor Init(aX, aY, aWidth, aHeight:integer; aName:string);
      procedure Show; virtual;
      procedure Hide; virtual;
      function Event:boolean;
      procedure Select; virtual;
      procedure DeSelect; virtual;
  end;
  TLineButton = object (TButton)
    public
      Procedure Show;  virtual;
      procedure SetName(aName:string);
      procedure Select; virtual;
      procedure DeSelect; virtual;
  end;
  TTextField = object (TLineButton)
    public
      procedure Show; virtual;
      function Fill(var aStr:string):char;
      function GetX: integer;
      function GetY: integer;
  end;
  
  procedure Error (aStr:string);
  procedure SetScreenMax;

implementation
const
  WindowColor=$C0C0C0;{Silver}
  TextColor=$000000;{Black}
  NameColor=$000000;{Black}
  ButtonColor=$D1BEAD;{Casper}
  FrameColor=$545454; {Dark Gray}
  CharSize=FontSize;

constructor TButton.Init(aX, aY, aWidth,
            aHeight:integer;aName:string);
begin
  X:=aX;
  Y:=aY;
  Height:=aHeight;
  Width:=aWidth;
  Name:=aName;
end;

procedure TButton.Show;
begin
  SetColor (FrameColor);
  SetFillStyle (SolidFill, ButtonColor);
  FillRect(X, Y, X+Width, Y+Height);
  SetColor (TextColor);
  SetTextJustify (CenterText, TopText);
  OutTextXY (X+Width div 2, Y+ (Height) div 4, Name);
  If Selected then begin SetColor (FrameColor);
    SetLineStyle (DottedLn, 0 ,NormWidth);
    rectangle (X+2,Y+2,X+Width-2,Y+Height-2);
    SetLineStyle (SolidLn, 0, NormWidth);
  end;
end;

procedure TButton.Hide;
begin
  SetFillStyle (SolidFill, WindowColor);
  bar (X, Y, X+Width, Y+Height);
end;

procedure TButton.Select;
begin
  Selected:=True;
  Show;
end;

Procedure TButton.DeSelect;
begin
  Selected:=False;
  Show;
end;

function TButton.Event:boolean;
begin
  if Selected then
    Event:=True 
  else
    Event:=False;
end;

procedure TLineButton.Show;
begin
  SetFillStyle (SolidFill, WindowColor);
  bar (X, Y, X+Width, Y+Height);
  SetColor (DarkGray);
  Line (X, Y, X+Width, Y);
  Line (X, Y+Height, X+Width, Y+Height);
  SetColor (TextColor);
  SetTextJustify (CenterText, TopText);
  OutTextXY (X+Width div 2, Y+ (Height) div 4, Name);
  If Selected then begin SetColor (FrameColor);
    SetLineStyle (DottedLn, 0 ,NormWidth);
    rectangle (X,Y+1,X+Width,Y+Height-1);
    SetLineStyle (SolidLn, 0, NormWidth);
  end
end;

procedure TLineButton.SetName (aName: string);
begin
  Name:=aName;
end;

procedure TLineButton.Select; 
begin
  Selected:=True;
  Show;
end;

Procedure TLineButton.DeSelect;
begin
  Selected:=False;
  Show;
end;

const FieldColor=$FFFFFF;

procedure TTextField.Show;
begin
  SetColor (FrameColor);
  SetFillStyle (SolidFill, FieldColor);
  FillRect (X, Y, X+Width, Y+Height);
  SetColor (TextColor);
  SetTextJustify (LeftText, TopText);
  OutTextXY (X+7, Y+ (Height) div 4, Name);
  
  If Selected then begin SetColor (FrameColor);
    SetLineStyle (DottedLn, 0 ,NormWidth);
    rectangle (X+1,Y+1,X+Width-1,Y+Height-1);
    SetLineStyle (SolidLn, 0, NormWidth);
  end
end;

Type 
  TBanSymb = set of char;
const BanSymb : TBanSymb = ['?','<','>','\','|','/', ':','*','"'];

function TTextField.Fill(var aStr:string):char;
var ch:char; 
begin
  select;
  SetTextJustify (LeftText, TopText);
  ch:=#00;
  repeat
    ch:=readkey;
    Hide;
    case ch of
      Down: Fill:=Down;
      Up: Fill:=Up;
      else begin
        if ch = BkSpase then
          Delete (Name,Length(Name),1)
        else
        if (Ord (ch)>=32)  and (ord(ch)<128) and not (ch in Bansymb)then
          Name:=Name+ch;
        Show;
      end;
      
    end;
  until (ch=Enter) or (Fill=Down) or (Fill=Up);
  aStr:=Name;
  deselect;
  show;
end;

function TTextField.GetX: integer;
begin
  GetX:=X;
end;
function TTextField.GetY: integer;
begin
GetY:=Y;
end;

Procedure SetScreenMax;
begin
  setviewport (0,0,GetMaxX,GetMaxY,ClipOn);
end;

procedure Error (aStr:string);
begin
  SetColor (Red);
  SetTextJustify (CenterText, TopText);
  OutTextXY (GetMaxX div 2, GetMaxY-TextHeight(aStr), aStr);
  readkey;
  readkey;
  halt;
end;
end.
